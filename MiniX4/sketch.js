// declaring variables
let checkboxYes;
let checkboxNo;

// loading images
function preload() {
  stitch = loadImage('stitch.png')
  stitchSleeve = loadImage('stitchSleeve.png')
  suitcase = loadImage('suitcase.png')
  sleeves = loadImage('sleeves.png')
  schoolbag = loadImage('schoolbag.png')
}

function setup() {
  // put setup code here
  createCanvas(600, 500);

  // Creating CheckBoxYes
  checkboxYes = createCheckbox('Yes'); 
  checkboxYes.position(75,300);

  // Creating CheckboxNo
  checkboxNo = createCheckbox('No'); 
  checkboxNo.position(415, 300);
 }
 
 function draw() {
    background(255,125,170); // color of background

    // creating a grey rectangle
    fill('lightGrey');
    noStroke();
    rect(70, 130, 400, 200);

    // placing the image with the defined name "stich" at the given coordinates
   image(stitch, 205, 234, 140, 100);
   
   // creating a white rectangle with a black stroke
   stroke('black');
   fill('white');
   rect(75, 135, 385, 40);

   // creating a black text with the textsize 24
   noStroke();
   fill(0);
   textSize(24);
   text('Do you want to accept our cookies?', 78, 164);
   
   // Changing the textsize to 6 and filling every object afterwards with grey
   textSize(6);
   fill('grey');
   text('By accepting our cookies, you agree to our terms and conditions. You agree to us gaining access to the wonderful world of data. We receive your data and in exchange, we give you access to our website. Where you will be able to do your work and use your time ambitiously, without any distractions and what so ever. The data we receive is your data, which means that we of course respect that it is yours. We will of course not be doing everything in our power to protect it, because our employees are currently occupied. We hope you do not mind, that while we are in possession of your data, we will sell your data to other companies, and they will properly use it to track your interests and private life. We will all get to know you even better than you do yourself and of course, we will now know exactly what you want to spend your money on. We will know: When you sleep, when you eat, when you breathe, who your friends and family are, where you live and where you work, and your education status. We will get to know everything about you. In the end, you will almost be able to consider us  as your best and closest friend, because we will get to know you even better than your friends do!', 75, 180, 390);

   if (checkboxYes.checked()) {
    // Creating a rectangle with the background color 
    fill(255,125,170); 
    rect(70, 130, 400, 205);
    // Creating the white rectangle
    fill('white');
    rect(55, 140, 500, 220, 20); // x, y, width, height, roundness
    
    image(schoolbag, 0, 0, 140, 140);
    image(sleeves, 460, 0, 140, 140);
    image(stitchSleeve, 0, 360, 140, 140);
    image(suitcase, 460, 360, 140, 140);

    // Creating the text in the buttom of the screen
    fill('black');
    text('Thank you! I am now tracking your locations, checking your private messages, checking your personal information, your age, your gender, your socialstatus and how often you sleep and breath! :)', 230, 475, 200, 200);
    textSize(30);
    // "FOR SALE!!" texts
    text('FOR SALE!!', 0, 50, 700);
    text('FOR SALE!!', 430, 50, 700);
    // The "FOR SALE!!" texts in the buttom of the screen
    fill('white');
    text('FOR SALE!!', 0, 420, 700);
    text('FOR SALE!!', 430, 420, 700);
    // Creating the big advertisement-text on the screen 
    fill('black');
    textSize(50);
    text('IF YOU BUY ALL TODAY YOU GET FREE SHIPPING!!!', 90, 165, 565);
    
    checkboxNo.hide();
    checkboxYes.hide();

    } else if (checkboxNo.checked()) {
      // red rectangle behind text
      fill('red');
      rect(100, 180, 500, 120);
  
      // creating the black "ERROR text"
      fill('black');
      textSize(100);
      text('ERROR!!!!', width / 5.5, height / 1.8); 
      textSize(20);
      
      // Creating the text that follows the user's mouse position
      text('Please refresh and change your mind :)', mouseX, mouseY);
      
      // Hiding the checkboxes
      checkboxYes.hide();
      checkboxNo.hide();
   }
  }   
