# MiniX4 - _Programmed by Julie_
**This is my _"ReadMe"-file._**  
It is used to describe my experinces with programming and what my purpose with my code is. It is also used to describe what my code does and how it functions. 

**Link to view my code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX4/MiniX4sketch.js?ref_type=heads)

**Link to run the code**
https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX4/index.html

![](BuyMeProductVideo_mov.mov)

## Describtion of my program
**What does it do?**.  
My program works as a pop-up and asking the user if they want to accpet the website's cookies. The user is provided with two options: yes and no. Underneath the question "Do you want to accept our cookies?" lays a describtion of what the user agrees to by accepting the cookies. The text is created so small and is so long and that is on purpose. The intention of this to make the user not wanting to read it and just continue to click "yes" instead. 

- What happens if the user clicks yes?
If the user clicks yes the users gets their screen filled with advertisements, because on the user's data that they have just delivered to the website. 

- What happens if the user clicks no?
If the user clicks "no" and thereby not accepts the cookies an "ERROR" message pops up and a text follows the user's mouse where it says "Please refresh and change your mind :)". The error message is placed chaotically on the screen. By this I mean it is for instance not placed in the middle of the screen and it is placed above the "cookie-question"-box from earlier. This is done on purpose to make it look unbalanced and make the user "uncomfortable", and try pushing the user to refresh the website and change their mind.

**What have I used?**
In my code I have used images, checkboxes, rectangles, texts and if-statements.

I focused a lot in this assignment on the placement of different objects like texts, images and rectangle in order to get different kinds of rectangles behind texts and texts infront of the different kinds of images. 

**What have I learnt?**
I have learnt how to hide and show objects. In this assignment particullay checkboxes. By writing the following:
```
if (checkboxNo.checked()) {
    fill('red');
    rect(100, 180, 500, 120);
    fill('black');
    textSize(100);
    text('ERROR!!!!', width / 5.5, height / 1.8);
    textSize(20);
    text('Please refresh and change your mind :)', mouseX, mouseY);
    checkboxYes.hide();
    checkboxNo.hide();
    } else {
  }
```
by using "checkboxYes.hide();" I could hide the checkbox I have created. This made me able to fix an error I made in the beggining when I started programming my code, where I had created the two checkboxes, but I could not make them dissapear. If I could not make them dissapear it gave the user the ability to check both checkboxes and therefore made the different texts and images etc. overlap each other and appear at the same time on the screen, which was not my intention. 

# How does my program fit the theme "_capture all_"?
My program fits the theme "_capture all_ by showing how much data the website resive after the user have accepted the cookies. In the buttom of the screen (when the user have agreed to the cookies) there is a small text-describtion which describes all the information the user gives the website now. I have exaggerated this by telling the user that the website now knows when they sleep and breath along with other "normal" information other websites commonly resives from users. 

# What are the cultural implications of data capture?
By allowing websites to resive all of our data like how many times we use on the social medias, the social medias can shape how and how much we use in our daily life.
For instance:
If I go into Instagram and I watch an Instagram reel the Instagram algorithm tracks how long time I use watching this reel "Do I watch it to the end or do I skip it?" If I watch it to the end the algorithm from now on shows me reels that have some common traits as the other reel I had watched. The Instagram algorithm keeps doing this all the time and therefore I might accidently use more of my time on Instagram than I orgianally wanted to. 

# Reference
**Pictures**
https://m.media-amazon.com/images/I/616MC2ISG+S._AC_SL1500_.jpg 
https://cdn.printerval.com/unsafe/960x960/asset.prtvstatic.com/2023/02/28/63fdd0e63b9897.19574930.jpg

https://m.media-amazon.com/images/I/41FKtVHPCIL._AC_UF894,1000_QL80_.jpg

https://i5.walmartimages.com/seo/StorageBud-20-inch-Hardside-Carry-On-Expandable-Luggage-Front-Pocket-Luggage-Set-Spinner-Suitcase-Set-Black_54da50a5-8b99-4474-ab43-d0fdc885d052.011e2a507a90af0d0849349c4af4fcb2.jpeg?odnHeight=768&odnWidth=768&odnBg=FFFFFF 

https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRO7MD-ZBnvldgZfiWlAOn31nO0cRF2NB_WSQ&usqp=CAU 


