let originalSnake;
let scaleSize = 40; // Values of the scaleSize to i.e. snake, food etc.
let food;

function setup() {
  createCanvas(600,600);
  originalSnake = new Snake();
  frameRate(10);
  pickLocation(); 
 }
 
 // Calling all the method in the Snake-class.
 function draw() {
  background(250,153,176);
  createBackground();  

  originalSnake.death();
  originalSnake.update();
  originalSnake.show();
  
  if (originalSnake.eat(food)) { // if method originalSnake.eat(food) === true, then recall function pickLocation
    pickLocation();
  }

  // Drawing and positioning the food
  fill(255, 0, 100);
  circle(food.x, food.y, scaleSize, scaleSize);
  }
  
  // Creating the background
  function createBackground (){
    let numberOfRectangles = window.width;
    let distanceBetween = 14;
    let x2 = 0;
    let y2 = 0;
    let d2 = 20;
    let rows2 = 43;

  // Drawing the circles in the background
  for (let i = 0; i < rows2; i++) {
  for (let j = 0; j < numberOfRectangles; j++) {
    fill(249, 187, 201, 170);
    noStroke();
    rect(x2, y2, 10, 10, 50);
    x2 += distanceBetween;
    }
  x2 = 0;
  y2 += distanceBetween;
  }
}

  // Makes the food perfectly allign with the snake
 function pickLocation() {
  let cols = floor(width/scaleSize); // 15
  let rows = floor(height/scaleSize); // 15
  food = createVector(floor(random(cols)), floor(random(rows)));
  // vector describes which direction the food are moving in
  food.mult(scaleSize); // multiply function which ensures the food's position is aligned with the grid
 }
  
  // Creating the interaction elements for the user to move the snake around
  function keyPressed() {
    if (keyCode === UP_ARROW) {
        originalSnake.direction(0, -1);
    } else if (keyCode === DOWN_ARROW) {
        originalSnake.direction(0, 1);
    } else if (keyCode === RIGHT_ARROW) {
        originalSnake.direction(1, 0);
    } else if (keyCode === LEFT_ARROW) {
        originalSnake.direction(-1, 0);
    }
  }