class Snake {
    constructor(x, y) {
      // All of the properties for the class "Snake"
    this.x = 0;
    this.y = 0;
    this.xspeed = 0;
    this.yspeed = 0;
    this.total = 0;
    this.tail = [];
  }
  
  // Sets the Snake's movement direction
  direction(x, y) {
    this.xspeed = x;
    this.yspeed = y;
  }

  // Checks if the Snake has eaten its own tail - Resets
  death(){
    for (let i = 0; i < this.tail.length; i++) {
      let pos = this.tail[i];
      let d = dist(this.x, this.y, pos.x, pos.y); // pos.x and pos.y reference the current positon of the tail
      if (d < i) {
        this.total = 0;
        this.tail = [];
      }
    }
  }

  // Updates the snake's position to align with the grid and manages the tail's length
  update() {
    if (this.total === this.tail.length) {
      for (let i = 0; i < this.tail.length-1; i++) {
        this.tail[i] = this.tail[i+1];
      }
    }
    this.tail[this.total-1] = createVector(this.x, this.y);
   
    this.x = this.x + this.xspeed*scaleSize; // 50 + 1 * 40
    // ganger med ScaleSize for at få den til at bevæge sig direkte over food
    this.y = this.y + this.yspeed*scaleSize;

    this.x = constrain(this.x, 0, width-scaleSize); // constrains the "snake" to be inside the width of the canvas
    this.y = constrain(this.y, 0, height-scaleSize); // constrains the "snake" to be inside the height of the canvas
  }

  // Draws the snake and its tail
  show() {
    fill('lightgreen');
    noStroke();
    for (let i = 0; i < this.tail.length; i++) {
    circle(this.tail[i].x, this.tail[i].y, scaleSize, scaleSize); // draws the "tail" --> the food it has consumed
    }
    
    circle(this.x, this.y, scaleSize, scaleSize); // the body
    fill('black');
    circle(this.x-12, this.y, scaleSize/3, scaleSize/3); // the black eye to the left
    fill('white');
    circle(this.x-14, this.y, scaleSize/9, scaleSize/9); // the white pupil in the left eye
    fill('black');
    circle(this.x+12, this.y, scaleSize/3, scaleSize/3); // the black eye to the right
    fill('white');
    circle(this.x+14, this.y, scaleSize/9, scaleSize/9); // the white pupil in the right eye
  }

  // Checks if the Snake has "eaten" any of the food and increases the length of the tail afterwards
  eat(pos) {
    let d = dist(this.x, this.y, food.x, food.y);
    if (d < 1) {
      this.total++; // total goes up with 1.
      return true;
    } else {
      return false;
    }
  }
}
