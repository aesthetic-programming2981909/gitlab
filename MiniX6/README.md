# MiniX6 - _Programmed by Julie_
**This is my _"ReadMe"-file._**  
It is used to describe my experinces with programming and what my purpose with my code is. It is also used to describe what my code does and how it functions. 

**Link to view my code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX6/MiniX6sketch.js?ref_type=heads)

**Link to run the code**
https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX6/index.html

![](snakeGame.mov)

## What is the game about and how do you play it?
The program is an inspiration from the orignal snake game.
You move your "snake" around with up, down, right, and left arrow on your keyboard. The point of the game is to gain "food" to make your snake bigger. If your snake accidently goes into (or in other words "eats") itself, then you lose the game and have to start over.

## What are the properties and behaviors of objects?
The properties of the snake is it positions (_this.x_, _this.y_) and its speed (_this.xspped_, _this.yspeed_), its tail and its "total" ("total" means how large/big the snake is).
The behavior of the snake is then "direction", which makes the snake move in different directions. And "death" is another behavior of the snake. It makes the snake "die" and gets the player to start over and lose the game. And then there is the "update" behavior which makes the snake larger or biggger, checks if the snake is inside the canvas, and makes the snake move in one direction.
Then there is "show" behavior. This one creates the color, shape, and tail length for the snake.
The last behavior there is, is the "eat" behavior. This one makes it possible for the snake to "eat" the fruit, by checking the distance between the snake's x and y-position and the food's x and y-positions.

## What does the work express?
The work express the abstraction of a snake-object in the snake-game. In this game the snake-objekt can move around, eat, and then afterwards get bigger by eating.

## Which part of the design you like the most and why?
I like that it is possible for the snake to get bigger/larger by eating the food, and then after having eaten the food, the food disappear. 

## How do objects interact with the world?
Objects represents the relations and interactions between the real world and with the computatational. By for instance being that objects are representations of the real world. You take a piece of the real world and make a representation by creating an object abstraction in computing. This process is never neutral, because the representation is seen from the programmer's point of view. This is for instance seen in the way that the properties and behaviors selected for the object are what the programmer think is important to be included in the class of the object. 

## How do worldviews and ideologies built into objects' properties and behaviors?
By creating objects inside a code, people can either accidently or on purpose create racial and genered objects or properties belonging to the object. As the "_Aesthetic Programming: A Handbook of Software Studies_ by Soon, Winnie & Cox, Geoff says: "_Worldviews can often be unethical, and we only need to think of game-worlds to see poor examples of racial and genered abstraction that expose some of the assumptions of the world, and what properties and methods that these characters are bebing defined._" as being told in this quote, objects can uptain specific properties/behaviors which can in the end made the object appear racist by for instance making it appear in a very bad streotypically way. 

# Reference
- https://www.youtube.com/watch?v=AaGK-fj-BAM&list=PLRqwX-V7Uu6aRpfixWba8ZF6tJnZy5Mfw&ab_channel=TheCodingTrain 

- Soon, Winnie & Cox, Geoff. Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press, 2020.
