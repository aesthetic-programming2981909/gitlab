# MiniX2 - _Programmed by Julie._
**This is my _"ReadMe"-file._**  
It is used to describe my experinces with programming and what my purpose with my code is. It is also used to describe what my code does and how it functions. 

**Link to view my code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX2/MiniX2sketch.js?ref_type=heads)

**Link to run the code** https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX2/index.html

## Describtion of my program, what have I used, and what have I learnt:
**My program and what have I used?**

My program contains two emojis. 
The one one the left side changes color from time to time without the using having to interact with the program. Meanwhile there is a button above it, which changes the expression of the emoji.
The emoji can change into three different types of expressions: happy, sad and neutral.

The secound emoji (the one on the right side) is grey. This one can change shape if one presses the button above the emoji. It can change into three different types of shapes: circle, triangle and a rectangle. 

![](Emoji_video.mov)

**What have I learnt?**
I have learnt how to use and create arrays, and I have gotten a better understanding of how the control flow structures by using "if" conditions and "else" to make the left side emoji's expression change and make the right side emoji's shape change. 

```
if (currentFace === "circle") {
    fill('Grey');
    circle(500, 250, 200);
  } else if (currentFace === "triangle") {
    fill('Grey');
    triangle(400, 350, 500, 150, 600, 350);
  } else if (currentFace === "rectangle") {
    fill('Grey');
    rect(400, 150, 200, 200);
}
```

Other than that I have learnt to experience more with creating different kind of shapes. The mouth on the "sad" expression was for instance one of those things I used hours to try to get right. 

## How would I put my emojis into a wider social and cultural context that concerns a politics of representation, identity, race, colanilism, and so on?
**The first emoji** 
If we start with a dicussion about the first emoji (the one on the left side).
This one changes color all the time and the user have the opportunity to change the expression. This emoji properly apply to more users the the secound one does, because of the fact, that this emoji both changes color and you can change the expression as you like. 

I added the color-function (the function which makes the emoji change color from time to time automatically), to apply to universal users. So no one has to be concerned about their skin-color not being there, because all of the colors are randomized. 

The point of the color-change is not to make it match with your skin-color, but more make it match to one's personality. Like for instance if one's favorite color is pink, then they might want the emoji to become pink.

I have programmed it in a way so the colorchange happens slowly. I didn't want to user to have the possibility to change the colors for themselves, but rather to experince all of the different opportinties with different colors matching. However, it is still possible for the user to take a screenshout if the color they like would appear in the emoji, but again this is not rather the point. 

The point is to experience different colors matching with different expressions in an aesthetic way. 


**The secound emoji**
The secound emoji contains no color. It is always grey for all users.

I decided to make this emoji different, but making the color neutral (grey) and the expression neutral as well. 

However, with this emoji one has the ability to change the shape of the emoji. This was another way to try to apply to as many users as possible.

**Summarize**
But trying to make two different emojies which is trying to apply to all users was diffcult, but by making the colors randomize or neutral (grey) I feel like this could be a good attempt to make these emojis universal. 

Other than that the user does not feel appligated to feel a sertain kind of feeling while using this program, because the emoji on the left side can change their expression from happy to neutral to sad. 

