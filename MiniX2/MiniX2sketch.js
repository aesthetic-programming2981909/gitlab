// Defining variables and assigning them to the value "expression".
let expressions = ["sad", "neutral", "happy"];
// Making the Index of "currentExpressionIndex" start at 0.
let currentExpressionIndex = 0;

// Defining variables and assigning them to the value "faceTypes".
let faceTypes = ["circle", "triangle", "rectangle"];
// Making the Index of "currentFaceIndex" start at 0.
let currentFaceIndex = 0;


function setup() {
 // put setup code here
 createCanvas(windowWidth,windowHeight);
 //Makes the commands run at frameRate 1, which means that every command run with 1 secound.
 frameRate(2);

 //Creating a button with the id "expressionButton" and giving it the text "Change your 1st Emoji".
 let expressionButton = createButton("Change your 1st Emoji")
 // Defines the positon of the button.
 expressionButton.position(175, 80);
 // Creates the inside color of the button.
 expressionButton.style('background-color', '#FDADAD');
 // Makes the button use the function "changeExpression" when the user's mouse press it.
 expressionButton.mousePressed(changeExpression)
 // Creates a border around the button and gives it a surtain color.
 expressionButton.style('border','2px solid rgb(226, 123, 123');
 // Makes the button round.
 expressionButton.style('border-radius', '10px');

 // Creates a secound button above the 2nd emoji.
 let faceShapeButton = createButton("Change your 2nd Emoji");
 faceShapeButton.position(425, 80);
 faceShapeButton.style('background-color', '#B0C4DE');
 faceShapeButton.style('border', '2px solid rgb(112, 128, 144');
 faceShapeButton.style('border-radius', '10px');
 faceShapeButton.mousePressed(changeFaceType);
}

function draw() {
  // put drawing code here
  background(255);
  
  // Creates the 1st emoji's position, size, color (which it randomize with the use of the frameRate).
  noStroke()
  fill(random(0, 250), random(0, 250), random (0, 250))
  circle(250, 250, 200, 200);

  // Assigns expression and "currentExpressionIndex" to currentExpression.
  let currentExpression = expressions[currentExpressionIndex];

  // creates an "if"-statement, which defines that if the statement "sad" is true, 
  // then it should create the shapes defined down below.
  if(currentExpression === "sad") {
    fill('black');
    //mouth
    rect(200, 290, 100, 2);
    ellipse(200, 298, 2, 15);
    ellipse(300, 298, 2, 15);
    //eyes
    ellipse(200, 220, 10, 50); //left
    ellipse(298, 220, 10, 50); //right
    //tears
    fill('white');
    ellipse(298, 250, 5, 20)
    ellipse(200, 250, 5, 20)
    //inside detail of tears
    fill('lightblue');
    ellipse(298, 251, 4, 15)
    ellipse(200, 251, 4, 15)

    // continues with the "if"-statement, which here defines that if the statement
    // is false and instead if "neutral" then it should create the shapes
    // defined down below.
  } else if (currentExpression === "neutral") {
    fill('black');
    //mouth
    rect(200, 290, 100, 3);
    //eyes
    ellipse(200, 220, 20, 30); //left
    ellipse(298, 220, 20, 30); //right

    //the same as above just now with "happy" instead of "neutral"
  } else if (currentExpression === "happy") {
    fill('white');
    stroke('black');
    arc(250, 290, 100, 80, 0, PI);

    //eyes
    fill ('black')
    ellipse(200, 220, 20, 50); //left
    ellipse(298, 220, 20, 50); //right
  }


  let currentFace = faceTypes[currentFaceIndex];

  if (currentFace === "circle") {
    fill('Grey');
    circle(500, 250, 200);
  } else if (currentFace === "triangle") {
    fill('Grey');
    triangle(400, 350, 500, 150, 600, 350);
  } else if (currentFace === "rectangle") {
    fill('Grey');
    rect(400, 150, 200, 200);
}

fill('black');
  //mouth
  rect(450, 290, 100, 3);
  //eyes
  ellipse(475, 220, 20, 30); //left
  ellipse(525, 220, 20, 30); //right


}
// bottom

//creates a function which changes the expression from the index.
// every time the button is pressed it goes one further in the index.
function changeExpression() {
  currentExpressionIndex = (currentExpressionIndex + 1) % expressions.length;
}

function changeFaceType() {
  currentFaceIndex = (currentFaceIndex + 1) % faceTypes.length;
}

// mouse interactions

