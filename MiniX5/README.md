# MiniX5 - _Programmed by Julie_
**This is my _"ReadMe"-file._**  
It is used to describe my experinces with programming and what my purpose with my code is. It is also used to describe what my code does and how it functions. 

**Link to view my code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX5/MiniX5sketch.js?ref_type=heads)

**Link to run the code**
https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX5/index.html

![](randomnessPatterns.jpg)

# The rules of the program
1. The rectangles shall be filled with colors from the defined colorArray. The filling of these rectangles with the colors shall be random.
2. When the program has run and filled the entire canvas with the colorful rectangles and the blue rectangular lines, then it shall resets from the beginning.

# What have I created?
I have created a canvas which is suppose to symbolize a notebook-pattern. The note-book pattern first appear after all the retangles have been drawn, which create an effect like the rectangles are being filled with colors (just as if it were a person coloring the rectangles). The retangles color are randomnized between 4 colors to represent a sort of reality-aspect (because there are not many people who knows pencils/markers/pens etc. in all the colors in the world - therefore I chose to limit it to change between 4 colors).

# Main syntaxes in my code
**Arrays**  
Before running the code I have created a "colorArray". The colorArray creates 4 different colors the program can chose from (pink, lightblue, yellow, and lightgreen).
As I stated before I have created this colorArrayIndex to define specific colors for the program to chose from. 

**For-loop**  
Within my program I have created a for-loop, which is used to create these blue rectangles, with no fill inside, which shall reprents these small 
rectangular pattern, there is inside of a notebook. 
The for-loop is created in this way:
First it assigns the value 0 to x. Then it checks if x is smaller than width, if it is smaller then the width of the canvas
it runs the for-loop. When x is no longer smaller than the width of the canvas, the for-loop stops running.

**Conditional-statement**  
In the end of my code I have created two conditional-statement. The first conditional-statement checks if the x-value is less than the width of the canvas or greater than the canvas' width.
```
if (x < width) {
    x+= space
} else if (x > width) {
    x = 0
    y+= space
}
```

If the x-value is less then width, then it increases the value "x" with the value beloning to "space".
So for instance if x is equal to 5, then the value space (which value starts at 40), increses with 5. So 40 plus 5 equal to 45.
The new x-value is now 45. 
When the x is greater than the width of the canvas, it assigns x to 0 again and now increases the y-value with "space".

# Reflection on the randomness aspects
So a big aspect of this program is the "randomness" it contains. The patterns/art piece it creates when the whole code is exacuated (except for the last conditional-statement) is created from the randomness. So how does this affect the user?
Well in this case the user does not get a choice in which color they want the rectangle to be filled with (from the 4 defined colors).

Secoundly it can be consider that the art that is being made is being produced by the program. 

Everyday in our life we encounter something that is being "random". 

**First example of the randomness aspect in daily life**  
A good example from this is the weather outside in Denmark. Sometimes the weather is nice and the sun is shining outside and then at other times it rains. The weather is so random, that sometimes even the weather-apps designed specifically to inform people around in Denmark how the weather is going to be today, tomorrow and a whole week forward, and not always filling us with correct information. In other words sometimes the weather apps fails to "guess" how the weather is going to be outside. 
So how does people normally react to randomness? Well if we look at this example people are not very pleased with the weather. A lot of times I hear other people and (I often do so myself) discuss how bad the weather is, and that even though one checked one of the weather apps, we still could not in time "prepare" for the weather outside, which makes people irritated sometimes.

**Secound example of the randomness aspect in daily life**  
But another example where the randomness is actually a good thing for people, are when you for instance have to decide something within small decisions. it could be who gets to start in a boardgame. 

**Shall the randomness aspect be considered as a good or bad thing?**  
This aspect of randomness as being something good (because it can makes us not having to decide on hard decisions), is also what Nick Montfort et al. says in their chapter about "_randomness_" in their book "_10 PRINT CHR$(205.5+RND(1)): GOTO10_":
"_Life itself is full of randomness and the inexplicable, and it is no small wonder that children and adults alike consciously incorporate chance into their daily lives, as if to tame it._” so therefore it might be more correct to consider randomness to often be more assosiated with something positive, but it depends on the situation and the people who experience the randomness.

# References:
- _Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, (Cambridge, MA: MIT Press, 2012), 119-146._  

- _Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142 - chapter 5._

- _https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/floor_
