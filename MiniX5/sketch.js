// defining variables and assinging values to them.
let x = 0;
let y = 0;
let space = 40;
// declaring the variable "colorIndex"
let colorIndex;
let colorArray = ['pink', 'lightblue', 'yellow','lightgreen'];

function setup() {
createCanvas(windowWidth, windowHeight)
background(255, 255, 255);
frameRate(60); // setting the frameRate to 60.
}

// a for-loop created to place the rectangles with no fill and light-blue strokes
function draw() {
    for (let x = 0; x < width; x = x + space) {
        noFill();
        // the blue stroke color around the rectangles.
        stroke(153, 170, 255);
        rect(x, y, 40, 40);
    }
    
colorIndex = floor(random(0, colorArray.length))
// makes the index choose a random variable inside the colorArray.
 noStroke();
 fill (colorArray[colorIndex]);
 // fills the rectangle with the current element in the colorArray
 rect(x, y, 40, 40);

if (x < width) {
    x+= space
    // if "x" is greater than the "width", a new value is assigned to the variable "x"
} else if (x > width) {
    x = 0
    y+= space
    // a new value is assigned to the variable "y"
}

// "resets" the program
if (y > height) {
    background(255, 255, 255);
    // creates a new background on the canvas
    x = 0
    y = 0
}
}