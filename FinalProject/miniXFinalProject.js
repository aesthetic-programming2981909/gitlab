// Declaring variables for the background images
let bg; // Game screen
let bg0; // Startscreen

// Variable to track the current screen
let screen = 0;

// Declaring variables for Akuma gifs used throughout the program
let gif1;
let gif2;

// Declaring variable for video capture
let capture;

// Defining variables used to create the text animation
let textAnimate = 0; // Variable to animate text movement
let addition = 1; // Variable to control the direction of movement

// Defining typewriter effect variables
let currentCharacter = 0;
let currentString = ''; //Currently displayed portion of the string

// Declaring variables for the buttons and input-typing box
let exitButton;
let yesButton;
let noButton;
let continueButton;
let input;
let exitYesButton;
let exitNoButton;

// Defining variables which control visibility 
let questionsVisible = true;
let wrongAnswerVisible = false;
let exitMessageVisible = false;
let inputVisible = false;

// Questionarray and exitarray
let questionsArray = [];
let exitArray = [];

// Index to track the current question or exit-message being displayed
let currentQuestionIndex = 0;
let currentExitIndex = 0;

// Variable storing the current question or exit object
let currentQuestion;
let currentExit;

let lastGlitchTime = 0; //Last time the glitch effect occured
let glitchDuration = 50; // duration of the glitch effect in milliseconds
let glitchInterval = 1000; // interval between glitches in milliseconds

function preload() {
    // Loading JSON data for questions & exit-messages
    loadJSON('questions.json', jsonLoaded);

    // Loading sound and fonts
    music = loadSound('music.mp3');
    joystix = loadFont("joystix_monospace.ttf");
    karmatic = loadFont("ka1.ttf");

    // Loading images for the Akuma character and background images
    gif1 = createImg('normalAkuma.gif');
    gif2 = createImg('evilAkuma.gif');
    bg = loadImage("https://pbs.twimg.com/media/EARTR-RXYAAwMmk.png:large")
    bg0 = loadImage("https://media.istockphoto.com/id/1356593868/vector/seamless-background-in-pixel-art-dithering-backdrop-in-8-bit-style.jpg?s=612x612&w=0&k=20&c=x5EoOoe08lP8PGk0HBy8ijaIjHuHKWDkrbu0C511gKE=")

    // Creating video capture
    capture = createCapture(VIDEO);
    capture.size(510, 510);
    capture.hide(); // Hiding video until needed
}

// Callback function for when JSON data is loaded
function jsonLoaded(data) {
    questionsArray = data.questions;
    exitArray = data.exitMessages;

// Set the current- Exit/Question to the first exit message/question in the exitArray
    currentQuestion = questionsArray[currentQuestionIndex];
    currentExit = exitArray[currentExitIndex];
}

function setup() {    
    createCanvas(1000, 700);
    frameRate(20); // Changing the tempo of the animation of the text in the first screen
    music.loop(); // Repeating the music

    // Functions to create and style the buttons
    createExitButton();
    createYesButton();
    createNoButton();
    createContinueButton();
    createExitYesButton();
    createExitNoButton();

    // Creating input field for answering questions
    input = createInput(''); 
    input.size(180, 20);
    input.style('background-color', color(0, 0, 0));
    input.style('color', 'white');
    input.position(665, 580);
}

// Function to display video feed
function displayVideoFeed() { 
    image(capture, 650, 120, 300, 300); 
  }
  
function draw() {
    fill('white');

    // Check the current screen state

    screenChange();

        //Checking if wrong answer is visible
        if (wrongAnswerVisible) {
            questionsVisible = false;
            gif1.hide();
            gif2.show();
            
            // Hide the input field and clear its value
            inputVisible = false;
            input.hide();
            input.value(''); // Erases the ealier wrong answer which were given

            continueButton.show();

            // Wrong answer text
            textSize(20);
            textFont(joystix);
            textSize(25);
            textSize(20);
            text('Haha ... maybe you didnt understand my question?', 70, 510, 900);
        }
        
        if (exitMessageVisible) { // If exit message is visible
            fill('white');
            textSize(20);
            textFont(joystix);
            text(currentExit, 70, 510); // Display current exit message

        }
    }

    function screenChange() {
        if (screen == 0) { // If on the start screen
            background(bg0); // Display bg0
    
            // Hides all the gifs on the start screen
            gif1.hide(); 
            gif2.hide(); 
            // Hides all buttons on start screen
            exitButton.hide() 
            exitNoButton.hide();
            exitYesButton.hide();
            yesButton.hide();
            noButton.hide();
            input.hide();
            continueButton.hide();
    
            textSize(50);
            textFont(karmatic);
            text('PLAY WITH AKUMA FRIEND', 100, 400 + textAnimate); // Displaying the title text with animation
            textAnimate += addition // Updating the textAnimate variable to move the text
            // Check if the text animation has reached its upper or lower limit
            if (textAnimate >= 5 || textAnimate <= -5) { 
            // Reverse the direction of the animation
                addition *= -1
            }
    
            textSize(20);
            textFont(joystix);
            text('click to start', 400, 450);
    
        } else if (screen == 1) { // If on the game screen
            background(bg); // Display bg
            
            // Used to make the eyes appear white
            noStroke(); 
            rect(445, 205, 100, 40); 
    
            // Calling the VirtualFriend function placing gif1 on the screen
            virtuelFriend();
            gif1.show();
            
            // Calling functions to place certain elements on the screen
            speechBubble();
            nameTag();
            
            // Calling this function to make Amkuma avatar glitch
            handleGlitchEffect();
    
            if (questionsVisible === true) {
                exitButton.show()
                yesButton.show();
                noButton.show();
                input.show();
                continueButton.show();
                showQuestions();
            }
        }
    }


//Functions to draw and style the buttons
function createExitButton() {
    exitButton = createButton('EXIT')
    exitButton.position(850, 40);
    exitButton.size(80, 40);
    exitButton.mousePressed(nextExit);
    exitButton.style('background-color', 'white');
    exitButton.style('border', '2px solid lightblue');
    exitButton.style('color', 'lightblue'); // The color of the text on the button
}

function createYesButton(){
    yesButton = createButton('Yes'); 
    yesButton.position(665, 580);
    yesButton.size(80, 40);
    yesButton.mousePressed(nextQuestion);
    yesButton.style('background-color', color(0, 0, 0));
    yesButton.style('border', '2px solid white');
    yesButton.style('color', 'white');
}

function createExitYesButton(){
    exitYesButton = createButton('Yes'); 
    exitYesButton.position(665, 580);
    exitYesButton.size(80, 40);
    exitYesButton.mousePressed(confirmation);
    exitYesButton.style('background-color', color(0, 0, 0));
    exitYesButton.style('border', '2px solid white');
    exitYesButton.style('color', 'white');
}

function createNoButton() {
    noButton = createButton('No');
    noButton.position(800, 580);
    noButton.size(80, 40);
    noButton.mousePressed(nextQuestion);
    noButton.style('background-color', color(0, 0, 0));
    noButton.style('border', '2px solid white');
    noButton.style('color', 'white');
}

function createExitNoButton() {
    exitNoButton= createButton('No');
    exitNoButton.position(800, 580);
    exitNoButton.size(80, 40);
    exitNoButton.mousePressed(backToQuestions);
    exitNoButton.style('background-color', color(0, 0, 0));
    exitNoButton.style('border', '2px solid white');
    exitNoButton.style('color', 'white');
}

function createContinueButton() {
    continueButton = createButton('Continue');
    continueButton.position(800, 580);
    continueButton.size(80, 40);
    continueButton.mousePressed(continueButtonPressed);
    continueButton.style('background-color', color(0, 0, 0));
    continueButton.style('border', '2px solid white');
    continueButton.style('color', 'white');
}

// Styling the gifs
function virtuelFriend() {
    gif1.position(250, 45);
    gif1.size(450, 450);

    //gif2.position(-300, -300);
    //gif2.size(1500, 1500);

    gif2.position(170, 10);
    gif2.size(600, 600);

}

// Drawing the speechbubble
function speechBubble() {
    noStroke();
    let c = color(0, 0, 0);
    c.setAlpha(100); // Changes how transparent it is
    fill(c);
    rect(50, 450, 900, 200);
}

// Creating a display for the "Akuma" name to appear on
function nameTag (){
    if (currentQuestionIndex > 0){ // Show nameTag when Akuma has introduced itself
        rect(50, 400, 200, 50);
        fill('white');
        textSize(20);
        textFont(joystix);
        stroke('black');
        text('AKUMA', 105, 435);
    }
}

// Function to display the questions
function showQuestions() {
    fill('white');
    textFont(joystix);
    textSize(20);

    // This extracts a portion of the currentQuestion.question string starting from the 0th index (beginning of the string) up to currentCharacter index (exclusive).
    currentString = currentQuestion.question.substring(0, currentCharacter);
    text(currentString, 70, 510, 900);
    // Checks if the currentCharacter is less than the length of the currentQuestion and then add a speedvalue of 5
    if (currentCharacter < currentQuestion.question.length) {
        currentCharacter += 5; // Speed of writing effect
    }

    // "Text" = just normal text appearing on the screen
    if (currentQuestion.type === "text") {
            inputVisible = false;
            gif2.hide();
            gif1.show();
            capture.hide();
            yesButton.hide();
            noButton.hide();
            exitNoButton.hide();
            exitYesButton.hide();
            input.hide();
            continueButton.show();

    // "Free" = with the oppportunity to answer the question - same goes for "special" (special = "Who's your best friend?")
    } else if (currentQuestion.type === "free" || currentQuestion.type === "special") {
            inputVisible = true;
            gif2.hide();
            gif1.show();
            capture.hide();
            yesButton.hide();
            noButton.hide();
            exitNoButton.hide();
            exitYesButton.hide();
            continueButton.hide();
            input.show();

    // Just a "yes" or "no" question
    } else if (currentQuestion.type === "yesno") {
            inputVisible = false;
            gif2.hide();
            gif1.show();
            capture.hide();
            input.hide();
            continueButton.hide();
            exitNoButton.hide();
            exitYesButton.hide();
            yesButton.show();
            noButton.show();

    // Question about taking a video of the user
    } else if (currentQuestion.type === "video") {
            gif2.hide();
            gif1.show();
            inputVisible = false;            
            input.hide();
            yesButton.hide();
            noButton.hide(); 
            exitNoButton.hide();
            exitYesButton.hide();
            displayVideoFeed();
            continueButton.show();

    // The end of the question and the program
    }  else if (currentQuestion.type === "ending") {
            displayVideoFeed();
            gif2.show();
            gif1.hide();
            inputVisible = false;            
            continueButton.show();
            input.hide();
            exitNoButton.hide();
            exitYesButton.hide();
            yesButton.hide();
            noButton.hide();
            exitButton.hide();
    }
}

function keyPressed() {
    
    // Check if the input is visible and the Enter key is pressed
    if (inputVisible && keyCode === ENTER) {

        // If the current question is of type "special" and the input value (converted to lowercase) is neither "you" nor "akuma"
        if (currentQuestion.type === "special" && !(input.value().toLowerCase() === "you" || input.value().toLowerCase() === "akuma")) {

            wrongAnswerVisible = true; // Set the variable to true to display the wrong answer text

        } else {
            // If it's not a "special" type question or the input is correct, proceed to the next question
            input.hide();
            input.value('');
            nextQuestion();
        }
    }
}

function nextQuestion() {
    questionsVisible = true;

    // Updating the current question index
    currentQuestionIndex++;
    currentCharacter = 0;  // Reset animation for the new question

    // Check if current question index is within bounds
    if (currentQuestionIndex < questionsArray.length) {
        // Update current question
        currentQuestion = questionsArray[currentQuestionIndex];
    }
}

function mousePressed() { //for start screen
    if (screen == 0) {
        screen = 1
    }
}

function handleGlitchEffect() { 
    if (exitMessageVisible) {
        // Get the current time in milliseconds
        let currentTime = millis();
        // Check if enough time has passed since the last glitch
        if (currentTime - lastGlitchTime > glitchInterval) {
            // Hide gif1 and show gif2 to create a glitch effect
            gif1.hide();
            gif2.show();
            // After the glitch duration, hide gif2 and show gif1 again
            setTimeout(function() {
            gif2.hide();
            gif1.show();
            }, glitchDuration);

            // Update the last glitch time to the current time
            lastGlitchTime = currentTime;
        }
    }
  }

function nextExit() {
        // Check if there are more exit messages
        
        if (currentExitIndex < exitArray.length) {
        
        currentExit = exitArray[currentExitIndex]; 
            
        // Show the exit text and option buttons
        exitMessageVisible = true;
        exitNoButton.show();
        exitYesButton.show();

        // Hides text and user input
        continueButton.hide();
        input.hide();
        questionsVisible = false;
        exitButton.hide();
        
        // Yes button calls a function that increases exitIndex and no buttom calls a function that makes questions visible again
        
    } else {
    
        backToQuestions()
        
    }
}

    function confirmation() {
        currentExitIndex = currentExitIndex + 1;
        exitMessageVisible = false;
        nextExit();
    }

    function backToQuestions() {
        questionsVisible = true;
        exitMessageVisible = false;
    }
    
    function continueButtonPressed() {
        if (wrongAnswerVisible) {
          wrongAnswerVisible = false;
          questionsVisible = true;
          gif2.hide();

        } else { 
        questionsVisible = true;
        nextQuestion(); // Proceed to the next question after showing the question
      }
    }