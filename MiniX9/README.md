# MiniX9  
**This is my _"ReadMe"-file._**  
It is used to describe the flowcharts I have created alone and together with my study group.

**Flowchart for miniX6 - _The Snake game_**  
First I want to talk about the flowchart I have created alone for my miniX6.

**Link to view my code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX6/MiniX6sketch.js?ref_type=heads)

**Link to run the code**
https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX6/index.html

![](TheSnakeGamePart1.png)

![](TheSnakeGamePart2.png)

![](TheSnakeGamePart3.png)

![](TheSnakeGamePart4.png)

The flowcharts shows the progress for my miniX6, which is called "_The Snake Game_". It shows the code's structure and how to read the code. I have chosen miniX6 to make a flowchart out of, because I feel like this is the most complexed miniX I have created so far. 


**Next I want to talk about the flowcharts created together with my study group**   

# Idea 1 - Extinction is forever!   
Our first idea is called “Extinction is forever” which is a game where you will be trying to save an endangered species of your choice. There will be three species to choose from. Each species will come with facts of why they are endangered and after this the player will face choices of how to save the animal. The choices, that the user make, matter and will define how long the species can survive!

![](Idea_1_-_Extinction_is_forever.png)

# Idea 2 - Virtual friend  
Our second idea is called “Virtual friend”, where the user is introduced to a “friend” initially appearing cute and “friendly”. It asks the user personal but innocent questions gaining the trust of the user. The questions slowly turn more invasive and ominous, while the user is unable to avoid or exit the program. Once all questions have been answered the screen crashes, revealing that the “friend” has left with your stolen data and now has stolen the user's identity. 

![](Idea_2_-_Virtual_friend.png)

# ReadMe Questions

**_What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?_**  
One of the difficulties was trying to include enough information for other people to understand the flowcharts, while not overwhelming the people with too much unnecessary information. If we were to simply the steps too much it could lead to other people not understanding what our programs is about, while if we made the steps to complicated it could make the user confused or misunderstand our programs. 

**_What are the technical challenges facing the two ideas and how are you going to address these?_**  
In both our programs we will have to include many steps or levels that can result in different outcomes. In other words: The user will always have a minimum of two options and this will be one of our technical challenges, because by creating two options for every question the user answer, there will also within these two options behold two options and these two options will also maintain two options and so forth. We are going to try to address this challenge by keeping the questions, that the user has to answer, to a minimum (for instance only 3 - 5 questions).
Another technical challenges could be creating the buttons, sliders, and a box where the user can type their input (the last one is mostly conscering the idea number 2) and getting these to work correctly. 
The last difficulty we came across was to create the animation for the virtual friend, because we are short on time. We are going to try to adress this difficulty by searching on the internet after animations we can borrow. 

**_In which ways are the individual and the group flowcharts you produced useful?_**  
The group flowcharts are useful in a way that it can be used in some sort of brainstorm. It is a great way to get ideas written down and to see the opportunities of the ideas. The individual flowchart is useful in another way. It is most useful in a way that it can help one to understand their miniX assignment by breaking down the code, and investigating how it works. 

# References
- Soon, Winnie & Cox, Geoff. Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press, 2020.
