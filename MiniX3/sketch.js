// telling the program to preload the image "hourglass.jpeg"
// and assigning the loadingvalue with the variable timeglass.
function preload() {
  hourglass = loadImage('hourglass.jpeg');
  }
  
  function setup() {
    createCanvas(windowWidth, windowHeight);
    // telling the program to run at a 80 framerate.
    frameRate(80);
   }
  
   function draw() {
    background(255, 105, 180, 50); 
    // letting the number of circles being the same amount as the window.width
    // in other words: the circles adjust according to the size of one's window.
    let numberOfCircles = window.width;
    // defining the distance between each circle
    let distanceBetween = 220;
    // defining the x-position, y-position and the diameter of the circles.
    let x = 10;
    let y = 10;
    let d = 30;
    // defining how many rows of circles I want the program to draw.
    let rows = 5;
    
    //creating a for-loop
    
    // Let the index start at 0
    // Check if the index is less then the amount of rows
    // if the infex is less then the amount of rows then it should continue with the next
    // 10 lines of code.
    // After having executed the 10 lines of code it coes back to line 34
    // and increases the index-value with 1.
    // This continues intill the index number is lower then the number of rows
    // (which is 5). When the index number is lower then the amount of rows
    // it "jumps out" of the for-loop and stops running the code
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < numberOfCircles; j++) {
        fill(255, 255, 255, 30);
        noStroke();
        rect(x-10, y-35, 90, 50, 40); // The x-value needs to be "x-10", because otherwise it will appear in the same place as the text
        fill(255, 0, 0, 60);
        text('TIME', x, y);
        textSize(d);
        // rect(x, y, d);
        x += distanceBetween;
      }
      x = 10;
      y += distanceBetween;
    } 
    // uses the function "drawElements"
    drawElements();
    // makes the elements appear at the center of the user's screen
    translate(width / 2, height / 2);
    
    //creates the big, purple circle
    fill(51, 45, 128, 80); // fills the circle with color
    stroke('black'); // makes the stroke around the circle being black
    strokeWeight(2); // makes the weight of the stroke ("the size") being 2.
    circle(0, 0, 100, 100); // makes the circle the size 100 x 100.
  
    // creates the inner circle
    noStroke(); // creates no stroke for the circle
    fill(255, 255, 255, 40); // repition
    circle(0, 0, 50, 50); // repition
   }

   function drawElements() {
    // Creates a varible called "num" and assigns the number "100" to it
    // This controls the number of elements which are being draw
    let num = 100;
    
    push();
    translate(width / 2, height / 2); // Move things to the center of the canvas
  
    let cir = 360 / num * (frameCount % num); // Assigns the calculated degree the elements shall be drawn in
    rotate(radians(cir)); // Converting the degress into radians and makes it rotate around the circle
    noStroke();
    
    image(hourglass, 120, 120, 90, 90);
    
    // Creates a black dot in the middle of the two other circles
    fill(0, 0, 0); 
    ellipse(10, 3, 10); 
  
    pop();
   }
  
   // Makes the window width and height change to match the user's window.
   function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
   }
  