# MiniX3 - _Programmed by Julie._
**This is my _"ReadMe"-file._**  
It is used to describe my experinces with programming and what my purpose with my code is. It is also used to describe what my code does and how it functions. 

**Link to view my code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX2/MiniX2sketch.js?ref_type=heads)

**Link to run the code**
https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX3/index.html

![](Throbber_video.mov)

## – Describtion of my code
My code contains images, shapes, a for-loop, push and pop effects, and translate and rotate effects. I’m using the shapes and an image downloaded from Google to draw the desired shapes and aesthetics for my canvas.
The for-loop is used to create a lot of circles in the background with the text “TIME” as a symbol for how time-consuming these throbbers can be, and how much time a user wastes daily on looking at these throbbers. 
The push and pop effects are used to control the drawing style settings of my canvas. 
By using “push” I create a new “drawing style” which translates the next elements defined to the center and rotates 360 degrees. 
The “pop” is then used to return to the old “drawing style” used before the “push” line. 
Translate makes everything appear at the center of the canvas and rotate makes the element rotate in a circle.

```
function drawElements() {
  let num = 100;
  
  push();

  translate(width / 2, height / 2);
  let cir = 360 / num * (frameCount % num);
  rotate(radians(cir));
  noStroke();
  image(hourglass, 120, 120, 90, 90);
  fill(0, 0, 0); // fills it with the color white
  ellipse(10, 3, 10); 

  pop();
 }
```

## – What do you want to explore and/or express?
I want to express how time-consuming a throbber can be from the users’ point of view, and how we daily waste many hours just staring at a throbber on our screens. This I have done by creating an hourglass that rotates around in a circle like a throbber usually does. 
I have then created two circles to appear in the center of the canvas. I created a black circle to appear in the center of the two other circles. The two circles are supposed to look like an eye and the black circle is supposed to look like the pupil inside the eye, which is then to symbolize the user’s eyes constantly watching the throbber. 
The last thing I created was the text “TIME” and a circle to appear in the background. This was to symbolize how much time we daily waste staring at throbbers and being like a “wake-up” call to do something else productive instead of keeping an eye on the throbber.

## Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?
For me and possibly for most people we associate throbbers with waiting time and something being in progress. Often when we come across a throbber it means that something is progressing. Something is working in the background, that we as users do not get to see, but instead, we get to see the throbber. The programmer has decided that the user does not need to see the computer’s progress. 

But this also sometimes can become problematic. Due to the user not being able to see the computer working, the user gets frustrated. The user does not understand why all of a sudden, the computer does not want to “cooperate” with them. “Ugh, why is the computer working against me?! Come on, work! Now!” may be some thoughts the user gets while being met with the throbber. The user does not understand where the error is – why the computer suddenly needs some time to work, because the user cannot see the problem, which leads to confusion, stress, frustration, and sometimes anger. The question I want to discuss at the end of my reflection is: Would it be better if the user could see the problem? 
If the user could see the problem or the reason why the throbber appeared out of the blue, then maybe the user would reflect upon this to prevent it from happening in the future. But with that being said, it would also mean that the replacement of the throbber would have to be written or shown in a way that the user would be able to understand, which leads me back to the starting point which is: maybe the throbber is not as bad. We should perhaps just keep the throbber and just learn to respect the throbber and trust the computer knows what it is doing (trusting that all our progress is save). 

## Refrence:
https://p5js.org/reference/#/p5/push 

https://p5js.org/reference/#/p5/frameCount

https://stock.adobe.com/dk/images/icon-empty-hourglass-template-sandglass-isolated-on-a-transparent-background/313560201 _– hourglass image_



