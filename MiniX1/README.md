# MiniX1 - _Programmed by Julie._
**This is my _"ReadMe"-file._**  
It is used to describe my experinces with programming and what my purpose with my code is. It is also used to describe what my code does and how it functions. 

**Link to view my code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX1/MiniX1sketch.js?ref_type=heads/)

**Link to run the code**
! https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX1/index.html

## What have I created and why?
I have programmed some sort of abstract art piece (that's what I will call it throughout this ReadMe). I created this art piece for me to try out experincing with p5 code and have fun while doing so. In my daily life I draw a lot, so I wanted to express more of that creativity I use in my daily life. I wanted to create something that was aesthetic to look at, meanwhile still having some sort of function, one could interact with.

![](AbstractionArtPiece.mov)

## How did I create my code and how does it work?
I started out with having two functions created for me, when loading the code. "setup" and draw". I started creating all the commands in setup first.

**What is inside the "preload" function?**
In this function I have told the program to search for my image, which is named "art.jpg" and to afterwards upload it to my code.

**What is inside the "setup" function"?**.  
I started out with creating a canvas with the command "createCanvas(x,y)" with the size 500 x 500. Then I added a backgroundcolor with the use of the command (background (R, G, B)". The last command I added to the function "setup" was a command that created a functional button, which purpose was to change the backgroundcolor. The command looked like this:
```
let button = createButton('Click me');
  button.position(215,245)
  button.mousePressed(() => {
    let bg = random(['pink','lightblue','lightgreen','lightyellow']);
    background (bg);
  }); 
}
```

**What is inside the "draw" function?**
Inside of the "draw" function I define the circles, rectangles and images position, sizes and colors. 

## How is the coding process different from, or similar to reading and writing text?
When you write a text you reflect upon what you are writing while writing. When you read you think about what you are reading and try to understand it. 

But when you program it is as if you must combine what you know about reading and what you know about writing. You need to read and write while programming. 
You need to read to understand your code and in order to create it. When you have read about it, you create it (write it). When you have written your code, you read it through again. And if the code does not work as you wanted it to, you read it again. And again. A better word to use in this context is to "analyze" because you start to read every single line and analyze it to find out where the mistake you have made is, since your code does not seem to work. When you finally find that one mistake you made and you have corrected it, you start feeling a sense of relief, because finally your code is working, and it is amazing. It is another feeling you get in comparison to when being done with reading or writing a text. 

You feel proud of yourself because you can see the effect of your code _live_. You can  it working. You can acknowledge that all the time and hours you have used to create this code, finally paid off. You will be rewarded right after being done with your work, in comparison to when being done with reading/writing, where you rarely feel rewarded. First hours, days, weeks, mouths, years later you will feel rewarded. Because you can't see the effect it has had, when you used all that time reading that one textbook. When you program, you can see the effect right away, and this is where the difference lies.

