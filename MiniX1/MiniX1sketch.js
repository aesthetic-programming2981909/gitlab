let img;
let newColor = 'white';

function preload() {
  img = loadImage('art.jpg');
}

function setup() {
  createCanvas(500, 500);
  background(255, 100, 100); 

  ChangeBGButton()

  changeCircleColorButton()
}

function draw() {
  creatingCircles();
  creatingRectangles();
  creatingImages();
}

function creatingCircles() {
  noStroke();
  fill(newColor);
  circle(80, 80, 120);
  circle(240, 240, 200);
  circle(400, 400, 120);
}

function creatingRectangles() {
  fill(0, 0, 0);
  noStroke();
  rect(350, 30, 100, 100, 20);
  rect(30, 350, 100, 100, 20);
}

function creatingImages() {
  image(img, 360, 40, 80, 80);
  image(img, 40, 360, 80, 80);
}

function changeCircleColorButton() {
  let button = createButton('No! Click me!');
  button.position(200,275);

  button.mousePressed(changeCircleColor);
}

function ChangeBGButton() {
  let button = createButton('Click me');
  button.position(215, 230);
  
  button.mousePressed(changeBGColor);
}

function changeBGColor() {
  let bg = random(['pink', 'lightblue', 'lightgreen', 'lightyellow']);
  background(bg);
}

function changeCircleColor(){
  newColor = random(['red','blue','purple']);
}
