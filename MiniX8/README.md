# MiniX8 - _Programmed by Astrid, Kalisha, Pia & Julie_
**This is my _"ReadMe"-file._**  
It is used to describe my experinces with programming and what my purpose with my code is. It is also used to describe what my code does and how it functions. 

**Link to view the code:**
![Click here](https://gitlab.com/aesthetic-programming2981909/gitlab/-/blob/main/MiniX8/MiniX8sketch.js?ref_type=heads)

**Link to run the code**
https://aesthetic-programming2981909.gitlab.io/gitlab/MiniX8/index.html

![](JellyBellyBean.png)

## About the program

**What is the program about? Which API have you used and why?**  
Our program generates a random Jelly Belly Bean and displays information about it, including its flavor name and description, along with an image of the bean. BeanId is also used but not displayed. The beanId determines which image, background color and text color are displayed for the selected Jelly Belly Bean. The API we use is from Jelly Belly Wiki (https://jellybellywikiapi.onrender.com/api/beans), which gives us information about the different flavors. We used this API because we thought it would bring joy and color to whoever uses our program.

**Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data?**  
We acquired the data from the external API (https://jellybellywikiapi.onrender.com/api/beans). In the “preload” function we used the loadJSON() function to fetch data from the API asynchronously.
Once the data is retrieved, the gotData() function processes it. It selects a random item from the list of beans and extracts relevant information such as bean ID, flavor name, and description. The data is represented visually using images and text on the canvas. Each jelly bean flavor is visually represented by its image, flavor name, and description. Among the information we chose to leave out is whether the bean is sugar-free, gluten-free, seasonal or kosher.

**How much do you understand this data or what do you want to know more about?** 
It is significant to understand the structure and content of the data and inspect how it is organized, to manipulate and display it correctly. The option to check a “pretty print” option, optimizes the readability of the data. The data is relatively easy to understand, due to the properties being assigned simple, recognizable names such as “flavourName”.

**How do platform providers sort the data and give you the requested data?**  
Platform providers may sort and provide data based on various criteria, such as popularity, relevance, or randomness, depending on the API's design and purpose. Sorting algorithms or techniques may be employed by the provider to ensure fair and diverse data retrieval.

**What is the significance of APIs in digital culture?**  
The significance of APIs in digital culture is that we need to understand that all the data provided online is (specifically) selected by the programmers and the business company. When using an API some data is selected while some is absent, which means that we don’t get all the information acquired, but we only get the information that the programmer and company want us to acquire. The point is: Nothing is randomly created, everything is specifically chosen by the creators. For instance, in our project, we have access to all the information about the Jelly Belly Bean like what kinds of beanId the beans contain and are they sugar free or gluten free. For our project we wanted to gather knowledge about, what their flavor name was and the description of the Jelly Belly Bean.

**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**  
Are there any ways to find out if a specific website uses an API or multiple APIs?

# Reference
- Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186

- https://www.youtube.com/watch?v=rJaXOFfwGVw&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&ab_channel=TheCodingTrain - Videos 10.1, 10.2, 10.3, 10.4, 10.5, 10.6, 10.7. 10.8