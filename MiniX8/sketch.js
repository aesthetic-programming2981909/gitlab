let bean;
let bean1Img;
let bean2Img;
let bean3Img;
let bean4Img;
let bean5Img;
let bean6Img;
let bean7Img;
let bean8Img;
let bean9Img;
let bean10Img;
let textColor;


function preload() {
  let apiURL = "https://jellybellywikiapi.onrender.com/api/beans";
  loadJSON(apiURL, gotData);
  bean1Img = loadImage('7UP.png')
  bean2Img = loadImage('CreamSoda.png')
  bean3Img = loadImage('RootBeer.png')
  bean4Img = loadImage('AcaiBerry.png')
  bean5Img = loadImage('ApplePie.png')
  bean6Img = loadImage('BarbadosCherry.png')
  bean7Img = loadImage('Blueberry.png')
  bean8Img = loadImage('BubbleGum.png')
  bean9Img = loadImage('ButteredPopcorn.png')
  bean10Img = loadImage('Cantaloupe.png')
  music = loadSound('jellysong.mp3');
  
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  music.loop();
  
}

function gotData(data) {
  let items = data.items;
  let randomIndex = floor(random(items.length));
  let randomItem = items[randomIndex];
  
  bean = {
    beanId: randomItem.beanId,
    flavorName: randomItem.flavorName,
    imageUrl: randomItem.imageUrl,
    description: randomItem.description
  }
}

function draw() {

  if (bean.beanId == 1) {
    background (229,255,204);
    image(bean1Img, 530, 200, 420, 280);
    textColor = color	(111,230,147);

  } else if (bean.beanId == 2){
    background (255,235,205);
    image(bean2Img, 530, 200, 420, 280);
    textColor = color(210,180,140);

  } else if (bean.beanId == 3){
    background (153,0,76);
    image(bean3Img, 530, 200, 420, 280);
    textColor = color	(0,0,0);

  } else if (bean.beanId == 4){
    background (153,0,76);
    image(bean4Img, 530, 200, 420, 280);
    textColor = color		(0,0,0);
    
  } else if (bean.beanId == 5){
    background(245,222,179);
    image(bean5Img, 530, 200, 420, 280);
    textColor = color(218,165,32);
    
  } else if (bean.beanId == 6){
    background ('pink');
    image(bean6Img, 530, 200, 420, 280);
    textColor = color('red');

  } else if (bean.beanId == 7){
    background (175,238,238);
    image(bean7Img, 530, 200, 420, 280);
    textColor = color(87,114,223);

  } else if (bean.beanId == 8){
    background (255,182,193);
    image(bean8Img, 530, 200, 420, 280);
    textColor = color(255,105,180);
    
  } else if (bean.beanId == 9){
    background (255,228,181);
    image(bean9Img, 530, 200, 420, 280);
    textColor = color(255,165,0);

  } else if (bean.beanId == 10){
    background (255,160,122);
    image(bean10Img, 530, 200, 420, 280);
    textColor = color(210,105,30);
}

  textFont('Lobster');
  textSize(60);
  textAlign(CENTER, CENTER);
  text('Get Yourself A Jelly Belly Bean!', windowWidth / 2, windowHeight / 8);
  
  textFont('ariel');
  textSize(30);
  textStyle(BOLD);
  text('Congratulations, you got:', windowWidth / 2, windowHeight / 5);

  fill(textColor);
  loadBean(); 
}

function loadBean() {
    text("Flavor Name: " + bean.flavorName, windowWidth / 2, windowHeight / 1.4);
    textStyle(ITALIC);
    text("Description: " + bean.description, windowWidth / 3.15, windowHeight / 1.25, 550);
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

